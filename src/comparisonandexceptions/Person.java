package comparisonandexceptions;

import java.util.Objects;

public class Person {

	private String firstName;
	private String lastName;
	private String personalCode;
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public String getPersonalCode() {
		return personalCode;
	}
	
	public Person(String firstName, String lastName, String personalCode) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.personalCode = personalCode;
	}
	
	@Override
	public int hashCode() {
		int firstNameHash = Objects.hashCode(this.firstName);
		int lastNameHash = Objects.hashCode(this.lastName);
		int personalCodeHash = Objects.hashCode(this.personalCode);
		return Math.abs(firstNameHash * lastNameHash * personalCodeHash);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		
		Person otherPerson = (Person)obj;
		
		return this.firstName.equals(otherPerson.getFirstName()) &&
				this.lastName.equals(otherPerson.getLastName()) &&
				this.personalCode.equals(otherPerson.getPersonalCode());
	}
	
}
