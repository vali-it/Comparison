package comparisonandexceptions;

public class Main {

	public static void main(String[] args) {
		
		Person p1 = new Person("Marek", "Lints", "12345");
		Person p2 = new Person("Marek", "Lints", "12345");
		Person p3 = new Person("Mati", "Tamm", "12346");
		
		System.out.println(p1.hashCode());
		System.out.println(p2.hashCode());
		System.out.println(p3.hashCode());
		System.out.println(p1 == p2);
		System.out.println(p1.equals(p2));
		System.out.println(p1.equals(p3));
		
//		String s1 = new String("tere");
//		String s2 = new String("tere");

//		String s1 = "tere";
//		String s2 = "tere";
		
//		String s1 = args[0];
//		String s2 = args[1];
//		
//		s1 = s1.intern();
//		s2 = s2.intern();
		
//		System.out.println(s1 == s2);
//		System.out.println(s1.hashCode());
//		System.out.println(s2.hashCode());
//		System.out.println(s1.equals(s2));
		
//		Object o1 = new Object();
////		System.out.println(o1.hashCode());
////		
//		Object o2 = new Object();
////		System.out.println(o2.hashCode());
////		
//		o2 = o1;
//		System.out.println(o1.hashCode());
//		System.out.println(o2.hashCode());
//		System.out.println(o1 == o2);
	}
}
